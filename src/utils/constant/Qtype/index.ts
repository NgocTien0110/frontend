const qtype = {
  essay: { code: "essay", vi_name: "tự luận" },
  short_answer: { code: "short-answer", vi_name: "trả lời ngắn" },
  multiple_choice: { code: "multiple-choice", vi_name: "nhiều lựa chọn" },
  true_false: { code: "true-false", vi_name: "đúng sai" }
};

export default qtype;
